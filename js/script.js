'use strict';

let personalMovieDB = {
    count: start(),
    movies: {},
    actors: {},
    genres: writeYourGenres(),
    privat: false,
}

rememberMyFilms();
detectPersonalLevel();
showMyDB(personalMovieDB.privat);






function start() {
    let numberOfFilms;
    do {
        numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?');
    }
    while (numberOfFilms == "" || numberOfFilms == null || numberOfFilms.length > 50 ||
        isNaN(+numberOfFilms) || !(Number.isSafeInteger(+numberOfFilms)) || numberOfFilms < 0)
    return numberOfFilms;
}

function rememberMyFilms() {

    let movieTitle;
    let filmRating;

    do {
        movieTitle = prompt('Один из последних просмотренных фильмов?');
    }
    while (movieTitle == "" || movieTitle == null || movieTitle.length > 50 || !(isNaN(+movieTitle)))
    do {
        filmRating = prompt('На сколько оцените его? (от 0 до 10)');
    }
    while (filmRating == "" || filmRating == null || filmRating > 10 || filmRating < 0 || isNaN(+filmRating))

    personalMovieDB.movies[movieTitle] = filmRating;
}

function detectPersonalLevel() {
    if (+personalMovieDB.count < 10) {
        console.log("Просмотрено довольно мало фильмов");
    } else if (+personalMovieDB.count < 30 && +personalMovieDB.count >= 10) {
        console.log("Вы классический зритель");
    } else if (+personalMovieDB.count >= 30) {
        console.log("Вы киноман");
    } else {
        console.log("Произошла ошибка");
    }
}

function showMyDB(condition) {
    if (!condition) {
        console.log(personalMovieDB);
    }
}

function writeYourGenres() {
    const genreTitle = [];
    for (let i = 0; i < 3; i++) {
        do {
            genreTitle[i] = prompt(`Ваш любимый жанр под номером ${i + 1}`);
        }
        while (genreTitle[i] == "" || genreTitle[i] == null || genreTitle[i].length > 50 || !(isNaN(+genreTitle[i])))
    }
    return genreTitle;
}